import * as express from 'express';

import { ServiceRegistry } from '../../helpers/serviceRegistry';
import { CustomerService, CUSTOMER_SERVICE } from '../../services/CustomerService';

export function get(
  serviceRegistry: ServiceRegistry
): express.Router {

  const router = express.Router();
  const customerService: CustomerService = serviceRegistry.get(CUSTOMER_SERVICE) as CustomerService;

  router.post('/customer/login',
    async (
      req: express.Request,
      res: express.Response,
      next: express.NextFunction
    ) => {
      try {
        const input = req as any;

        const email = input.body.email;
        const password = input.body.password;

        const authentication = await customerService.authorize(email, password);

        res.json(authentication);
      } catch (err) {
        err.message === 'Unauthorized' ? res.status(401).json({ message: 'Wrong credentials' }) : next(err);
      }
    });

  return router;

}
