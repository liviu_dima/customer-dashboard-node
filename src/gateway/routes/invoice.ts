import * as express from 'express';
import * as nconf from 'nconf';

import { ServiceRegistry } from '../../helpers/serviceRegistry';
import { INVOICE_SERVICE, InvoiceService } from '../../services/InvoiceService';
import { isAuthorized } from '../authorization';

export function get(
  serviceRegistry: ServiceRegistry
): express.Router {

  const router = express.Router();
  const invoiceService: InvoiceService = serviceRegistry.get(INVOICE_SERVICE) as InvoiceService;

  // protected routes
  const authorization = isAuthorized(nconf.get('authorization:secret'));

  router.get('/invoices',
    authorization,
    async (
      req: express.Request,
      res: express.Response,
      next: express.NextFunction
    ) => {
      try {
        let userId = (req as any).user._id;

        const input = req as any;

        const count = await invoiceService.getCustomerInvoicesNumber(
          userId,
          new Date(input.query.startDate),
        );

        const invoices = await invoiceService.fetchInvoicesForCustomer(
          userId,
          new Date(input.query.startDate),
          Number(input.query.page),
          input.query.perPage ? Number(input.query.perPage) : 20,
          count,
        );

        res.json({
          results: invoices,
          count: count,
        });
      } catch (err) {
        next(err);
      }
    });

  return router;

}
