import * as express from 'express';
import * as nconf from 'nconf';

import { ServiceRegistry } from '../../helpers/serviceRegistry';
import { isAuthorized } from '../authorization';
import { QuoteService, QUOTE_SERVICE } from '../../services/QuoteService';

export function get(
  serviceRegistry: ServiceRegistry
): express.Router {

  const router = express.Router();
  const quoteService: QuoteService = serviceRegistry.get(QUOTE_SERVICE) as QuoteService;

  // protected routes
  const authorization = isAuthorized(nconf.get('authorization:secret'));

  router.get('/quotes',
    authorization,
    async (
      req: express.Request,
      res: express.Response,
      next: express.NextFunction
    ) => {
      try {
        let userId = (req as any).user._id;

        const input = req as any;

        const count = await quoteService.getCustomerQuotesNumber(
          userId,
        );

        const quotes = await quoteService.fetchCustomerQuotes(
          userId,
          input.query.page ? Number(input.query.page) : 1,
          input.query.perPage ? Number(input.query.perPage) : 20,
          count,
        );

        res.json({
          results: quotes,
          count: count,
        });
      } catch (err) {
        next(err);
      }
    });

  router.post('/quotes',
    authorization,
    async (
      req: express.Request,
      res: express.Response,
      next: express.NextFunction
    ) => {
      try {
        let userId = (req as any).user._id;

        const input = req as any;

        const quoteId = await quoteService.saveQuote(userId, input.body.productId);

        res.json(quoteId);
      } catch (err) {
        next(err);
      }
    });

  return router;

}
