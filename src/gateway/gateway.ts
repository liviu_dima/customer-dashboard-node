import * as express from 'express';

import { ServiceRegistry } from '../helpers/serviceRegistry';

import { get as InvoiceRoutes } from './routes/invoice';
import { get as CustomerRoutes } from './routes/customer';
import { get as QuoteRoutes } from './routes/quote';

/**
 * Api gateway class.
 */
export class ApiGateway {
  public router: express.Router;

  /**
   * Class constructor.
   */
  constructor(
    public registry: ServiceRegistry,
  ) {
    this.router = express.Router();
    this.router.use(InvoiceRoutes(registry));
    this.router.use(CustomerRoutes(registry));
    this.router.use(QuoteRoutes(registry));

  }
}
