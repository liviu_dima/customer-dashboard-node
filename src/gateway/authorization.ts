import * as express from 'express';
import * as jwt from 'jsonwebtoken';

/**
 * Middleware used to check if a user is authorized to access a route (via JWT).
 */
export function isAuthorized(secret: string) {
  return function (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    res;
    const authorizationHeader = req.headers['Authorization'] || req.headers['authorization'];
    try {
      if (typeof authorizationHeader !== 'string') {
        throw new Error();
      }
      let token = authorizationHeader as string;
      if (!token) {
        throw new Error();
      }

      token = token.substr('Bearer '.length);
      let decoded = jwt.decode(token) as any;
      if (!decoded) {
        throw new Error();
      }

      jwt.verify(token, secret, (err, decodedToken) => {
        if (err) {
          throw new Error();
        } else {
          Object.defineProperty(req, 'user', {
            value: {
              _id: (decodedToken as any)._id
            }
          });
          next();
        }
      });
    } catch (err) {
      res.status(401).send({ error: 'Not authorized to access this resource' });
    }
  };
}
