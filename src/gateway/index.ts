import { ApiGateway } from './gateway';
export { ApiGateway } from './gateway';

import { ServiceRegistry } from '../helpers/serviceRegistry';

export function get(
  registry: ServiceRegistry
): ApiGateway {
  let gateway = new ApiGateway(
    registry
  );

  return gateway;
}
