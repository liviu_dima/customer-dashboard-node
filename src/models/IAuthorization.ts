export interface IAuthorization {
  id: number;
  role_id: number;
  username: string;
  salt: string;
  password: string;
  firstName: string;
  lastName: string;
}
