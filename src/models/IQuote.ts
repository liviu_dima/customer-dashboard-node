export interface IInvoice {
  id: number;
  total: number;
  deadline: Date;
  status: InvoiceStatus;
  paymentStatus: InvoicePaymentStatus;
  deliveryMethod: DeliveryMethod;
  forEvent: boolean;
  filePath: string;
  name: string;
}

export enum InvoiceStatus {
  accepted = 'accepted',
  new = 'new',
  pendingApproval = 'pending-approval',
}

export enum InvoicePaymentStatus {
  accepted = 'accepted',
  new = 'new',
  pendingApproval = 'pending-approval',
}

export enum DeliveryMethod {
  standard = 'standard',
  rush = 'rush',
  urgent = 'urgent',
}
