import { Connection } from 'mysql';

import { MySQLRepository } from './_mySQLRepository';
import { DateHelper } from '../helpers/dateHelper';
import { IInvoice } from '../models/IInvoice';

const TABLE_NAME = 'Invoice';

export class InvoiceRepository extends MySQLRepository<IInvoice> {

  constructor(
    protected _connection: Connection,
  ) {
    super(_connection, TABLE_NAME);
  }

  getInvoicesForCustomer(customerId: number, startDate: Date, offset = 0, limit = 20): Promise<IInvoice[]> {
    return new Promise<IInvoice[]>((resolve, reject) => {
      this._connection.query(`
      SELECT
        i0_.id AS id,
        SUM(p1_.total) AS total,
        MAX(p1_.deadline) AS deadline,
        i0_.status AS status,
        i0_.additionalAmountStatus AS paymentStatus,
        p1_.deliveryMethod AS deliveryMethod,
        i0_.forEvent AS forEvent,
        f2_.path AS filePath,
        i0_.name AS name
      FROM Invoice i0_
      INNER JOIN Placement p1_ ON p1_.invoice_id = i0_.id
      LEFT JOIN File f2_ ON p1_.id = f2_.placement_id
      LEFT JOIN File f3_ ON p1_.id = f3_.placement_id AND (f2_.id < f3_.id)
      WHERE i0_.customer_id = ?
      AND (p1_.created_date BETWEEN ? AND NOW())
      AND (p1_.isDead IS NULL OR p1_.isDead = 0)
      AND i0_.status IN ('accepted', 'new')
      AND f3_.id IS NULL
      GROUP BY i0_.id ORDER BY i0_.id
      DESC LIMIT ? OFFSET ?`, [
        customerId,
        DateHelper.getYMDate(startDate),
        limit,
        offset,
      ], (err, res) => {
        if (err)
          return this._connection.rollback(function () {
            return reject(err);
          });

        return resolve(res);
      });
    });
  }

  getInvoicesNumberForCustomer(customerId: number, startDate: Date): Promise<number> {
    return new Promise<number>((resolve, reject) => {
      this._connection.query(`
      SELECT count(DISTINCT i0_.id) AS count
      FROM Invoice i0_
      INNER JOIN Placement p1_ ON p1_.invoice_id = i0_.id
      LEFT JOIN File f2_ ON p1_.id = f2_.placement_id
      LEFT JOIN File f3_ ON p1_.id = f3_.placement_id AND (f2_.id < f3_.id)
      WHERE i0_.customer_id = ?
      AND (p1_.created_date BETWEEN ? AND NOW())
      AND (p1_.isDead IS NULL OR p1_.isDead = 0)
      AND i0_.status IN ('accepted', 'new')
      AND f3_.id IS NULL`, [customerId, DateHelper.getYMDate(startDate)], (err, [{ count }]) => {
        if (err)
          return this._connection.rollback(function () {
            return reject(err);
          });

        return resolve(count);
      });
    });
  }

}
