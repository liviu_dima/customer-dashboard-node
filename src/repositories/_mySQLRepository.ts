import { Connection } from 'mysql';

/**
 * MySQL Repository Base
 * to be extended by each table repo
 * @author Liviu Dima
 */
export class MySQLRepository<T> {

  constructor(
    protected _connection: Connection,
    private _table: string,
  ) { }

  async add(item: T): Promise<number> {
    return new Promise<number>((resolve, reject) => {
      this._connection.query(`INSERT INTO ${this._table} SET ?`, item, (err, res) => {
        if (err)
          return this._connection.rollback(function () {
            return reject(err);
          });

        resolve(res.insertId);
      });
    });
  }

  async findById(id: number): Promise<T> {
    return new Promise<T>((resolve, reject) => {
      this._connection.query(`SELECT * FROM ${this._table} WHERE id = ?`, [id], (err, res) => {
        if (err)
          return this._connection.rollback(function () {
            return reject(err);
          });

        !!res.length ? resolve(res[0]) : resolve(res);
      });
    });
  }

  async getAll(): Promise<T[]> {
    return new Promise<T[]>((resolve, reject) => {
      this._connection.query(`SELECT * FROM ${this._table}`, (err, res) => {
        if (err)
          return this._connection.rollback(function () {
            return reject(err);
          });

        resolve(res);
      });
    });
  }

  async deleteById(id: number): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this._connection.query(`DELETE FROM ${this._table} WHERE id = ?`, [id], (err) => {
        if (err)
          return this._connection.rollback(function () {
            return reject(err);
          });

        resolve();
      });
    });
  }

  async findBy(fields: Array<any>, values: Array<any>): Promise<Array<T>> {
    const whereConstraint = fields.map(field => `${field} = ?`).join(' AND ');
    const queryString = `SELECT * FROM ${this._table} WHERE ${whereConstraint}`;
    return new Promise<Array<T>>((resolve, reject) => {
      this._connection.query(queryString, [...values], (err, res) => {
        if (err)
          return this._connection.rollback(function () {
            return reject(err);
          });

        resolve(res);
      });
    });
  }
}
