import { Connection } from 'mysql';

import { MySQLRepository } from './_mySQLRepository';
import { IAuthorization } from '../models/IAuthorization';

const TABLE_NAME = 'CustomerTable';

export class CustomerRepository extends MySQLRepository<IAuthorization> {

  constructor(
    protected _connection: Connection,
  ) {
    super(_connection, TABLE_NAME);
  }
}
