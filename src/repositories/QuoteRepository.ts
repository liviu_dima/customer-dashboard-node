import { Connection } from 'mysql';

import { MySQLRepository } from './_mySQLRepository';

const TABLE_NAME = 'Quote';

export class QuoteRepository extends MySQLRepository<any> {

  constructor(
    protected _connection: Connection,
  ) {
    super(_connection, TABLE_NAME);
  }

  getQuotesForCustomer(customerId: number, offset = 0, limit = 20): Promise<any[]> {
    return new Promise<any[]>((resolve, reject) => {
      this._connection.query(`
      SELECT *
      FROM Quote
      WHERE customer_id = ?
      LIMIT ? OFFSET ?`, [
        customerId,
        limit,
        offset,
      ], (err, res) => {
        if (err)
          return this._connection.rollback(function () {
            return reject(err);
          });

        return resolve(res);
      });
    });
  }

  getQuotesNumberForCustomer(customerId: number): Promise<number> {
    return new Promise<number>((resolve, reject) => {
      this._connection.query(`
      SELECT count(id) AS count
      FROM Quote
      WHERE customer_id = ?`, [customerId], (err, [{ count }]) => {
        if (err)
          return this._connection.rollback(function () {
            return reject(err);
          });

        return resolve(count);
      });
    });
  }
}
