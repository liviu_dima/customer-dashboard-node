import * as nconf from 'nconf';
import * as mysql from 'mysql';

// helpers
import { Logger } from './helpers/logger';
import { ServiceRegistry } from './helpers/serviceRegistry';

// service
import { InvoiceService } from './services/InvoiceService';
import { CustomerService } from './services/CustomerService';

// repositories
import { InvoiceRepository } from './repositories/InvoiceRepository';

// gateway
import { get as ApiGatewayFactory, ApiGateway } from './gateway';
import { QuoteRepository } from './repositories/QuoteRepository';
import { QuoteService } from './services/QuoteService';
import { CustomerRepository } from './repositories/CustomerRepository';

/**
 * App class
 */
export class CustomerDashboard {
  public serviceRegistry = new ServiceRegistry(); // make it public for import usage
  public apiGateway: ApiGateway | undefined;

  private _mysqlConnection?: mysql.Connection;

  async start() {
    this._mysqlConnection = mysql.createConnection({
      host: nconf.get('mysql:host'),
      user: nconf.get('mysql:user'),
      password: nconf.get('mysql:password'),
      database: nconf.get('mysql:database'),
    });

    const invoiceRepo = new InvoiceRepository(this._mysqlConnection);
    const invoiceService = new InvoiceService(invoiceRepo);
    this.serviceRegistry.add(invoiceService);

    const customerRepo = new CustomerRepository(this._mysqlConnection);
    const secret = nconf.get('authorization:secret');
    const customerService = new CustomerService(
      secret,
      60 * 60 * 24 * 30,
      customerRepo
    );
    this.serviceRegistry.add(customerService);

    const quoteRepo = new QuoteRepository(this._mysqlConnection);
    const quoteService = new QuoteService(quoteRepo);
    this.serviceRegistry.add(quoteService);

    this._initGateway();

    Logger.get().write('*** App started ***');
  }

  private _initGateway() {

    this.apiGateway = ApiGatewayFactory(
      this.serviceRegistry,
    );
  }

  async stop() {
    if (this._mysqlConnection)
      this._mysqlConnection.end();

    Logger.get().write('*** App stopped ***');
  }

}
