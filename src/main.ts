import * as http from 'http';
import * as express from 'express';
import * as path from 'path';
import * as nconf from 'nconf';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';

import { ProcessManager } from './helpers/manager';
import { CustomerDashboard } from './app';
import { Logger } from './helpers/logger';

const pm = new ProcessManager();
let app: express.Express;
let server: http.Server;
let customerDashboard: CustomerDashboard;

pm.once('start', async () => {
  app = express();
  server = http.createServer(app);

  // wrap server with graceful shutdown capability.
  require('http-shutdown').extend();

  customerDashboard = new CustomerDashboard();
  await customerDashboard.start();

  // to be used only with a frontend (nginx or any load balancer)
  // (by default req.ip will ignore the 'x-forwarded-for' header)
  app.enable('trust proxy');

  // redirect http -> https
  app.use((req, res, next) => {
    // check for load balancer forwarded protocol header, not the direct protocol which will always be HTTP
    if (req.headers['x-forwarded-proto'] === 'http') {
      let host = req.hostname.replace(/^www\./i, '');
      let href = `https://${host}${req.url}`;
      return res.redirect(href);
    }

    let wwwRx = /^www\./i;
    if (wwwRx.test(req.hostname)) {
      let host = req.hostname.replace(/^www\./i, '');
      let href = `https://${host}${req.url}`;
      return res.redirect(href);
    }

    next();
  });

  // load balancer health check route
  app.get('/health-check', (req, res) => { req; res.end(); });

  // configure middleware
  app.use(bodyParser.json());
  app.use(cors({
    origin: [
      /localhost/i,
    ]
  }));

  // load API gateway
  if (!customerDashboard.apiGateway) {
    throw new Error('API GATEWAY NOT INITIALIZED.');
  }
  app.use(customerDashboard.apiGateway.router);

  let host: string = nconf.get('app:host');
  let port: number = nconf.get('app:port');
  server.listen(port, host, () => {
    Logger.get().write('magic happens on port', port);
  });

  // enable graceful shutdown on server
  (server as any).withShutdown();

  server.once('close', async () => {
    await tearDown();
  });
});

pm.once('stop', () => {
  Logger.get().write('received shutdown signal, closing server ...');

  (server as any).shutdown();
});
pm.init(path.join(__dirname, '../default.json'));

async function tearDown() {
  try {
    if (customerDashboard)
      await customerDashboard.stop();
    Logger.get().write('server closed gracefully.');

    // log();
  } catch (error) {
    Logger.get().error(error);
  }
}
