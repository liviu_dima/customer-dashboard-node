import { IService } from '../types/IService';
import { QuoteRepository } from '../repositories/QuoteRepository';

export const QUOTE_SERVICE = 'app:quote';

export class QuoteService implements IService {

  id = QUOTE_SERVICE;

  constructor(
    private _quoteRepo: QuoteRepository,
  ) { }

  async fetchCustomerQuotes(customerId: number, page: number, perPage: number, count: number) {
    const offset = perPage * (page - 1);
    perPage = perPage * page > count ? perPage - (perPage * page - count) : perPage;
    return await this._quoteRepo.getQuotesForCustomer(customerId, offset, perPage);
  }

  async getCustomerQuotesNumber(customerId: number) {
    return await this._quoteRepo.getQuotesNumberForCustomer(customerId);
  }

  async saveQuote(customerId: number, productId: number) {
    return await this._quoteRepo.add({ customer_id: customerId, product_id: productId });
  }

}
