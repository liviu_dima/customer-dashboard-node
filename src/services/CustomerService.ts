import { CustomerRepository } from '../repositories/CustomerRepository';
import { IService } from '../types/IService';
import { UserHelper } from '../helpers/userHelper';
import * as jwt from 'jsonwebtoken';
import { IAuthentication } from '../types/IAuthentication';

export const CUSTOMER_SERVICE = 'app:customer';

export class CustomerService implements IService {

  id = CUSTOMER_SERVICE;

  constructor(
    private _secret: string,
    private _accessTokenTtl: number,
    private _customerRepo: CustomerRepository,
  ) { }

  async authorize(email: string, password: string): Promise<IAuthentication> {
    const customers = await this._customerRepo.findBy(['email'], [email]);
    const customer = customers[0];

    if (!customer) {
      throw Error('Unauthorized');
    }

    const encodedPassword = UserHelper.encodePassword(password, customer.salt);

    if (encodedPassword !== customer.password) {
      throw new Error('Unauthorized');
    }

    const token = jwt.sign(
      { _id: customer.id },
      this._secret,
      { expiresIn: this._accessTokenTtl },
    );

    return {
      customerId: customer.id,
      firstName: customer.firstName,
      lastName: customer.lastName,
      email: email,
      bearer: token,
    };
  }

}
