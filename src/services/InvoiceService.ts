import { InvoiceRepository } from '../repositories/InvoiceRepository';
import { IService } from '../types/IService';

export const INVOICE_SERVICE = 'app:invoice';

export class InvoiceService implements IService {

  id = INVOICE_SERVICE;

  constructor(
    private _invoiceRepo: InvoiceRepository
  ) { }

  async fetchInvoicesForCustomer(customerId: number, startDate: Date, page: number, perPage: number, count: number) {
    const offset = perPage * (page - 1);
    perPage = perPage * page > count ? perPage - (perPage * page - count) : perPage;
    return await this._invoiceRepo.getInvoicesForCustomer(customerId, startDate, offset, perPage);
  }

  async getCustomerInvoicesNumber(customerId: number, startDate: Date) {
    return await this._invoiceRepo.getInvoicesNumberForCustomer(customerId, startDate);
  }

}
