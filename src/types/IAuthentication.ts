export interface IAuthentication {
  customerId: number,
  firstName: string,
  lastName: string,
  email: string,
  bearer: string,
}
