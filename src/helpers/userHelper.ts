
import * as crypto from 'crypto';

export class UserHelper {

  /**
   * Encode password the symfony way
   * @param password
   * @param salt
   */
  static encodePassword(password: string, salt?: string) {
    salt = !!salt ? salt : UserHelper.genSalt(31);
    const salted = `${password}{${salt}}`;
    let digest = crypto.createHash('sha512').update(salted).digest('latin1');
    for (var i = 1; i < 5000; ++i) {
      digest = crypto.createHash('sha512').update(Buffer.concat([Buffer.from(digest, 'binary'), Buffer.from(salted, 'utf8')])).digest('latin1');
    }

    return Buffer.from(digest, 'binary').toString('base64');
  }

  /**
   * Generate salt for new passwords
   */
  static genSalt(length: number) {
    let result = '';
    const characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return result;
  }

}
