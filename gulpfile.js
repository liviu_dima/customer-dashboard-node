const gulp = require('gulp');
const typescript = require('gulp-typescript');
const gulpTslint = require('gulp-tslint');
const tslint = require('tslint');
const sourcemaps = require('gulp-sourcemaps');


/**
 * Define tasks.
 */
gulp.task('clean', (cb) => {
  gulp.src(['build/'], { read: false }).pipe(rm());
  cb();
});

gulp.task('tslint', (cb) => {
  const program = tslint.Linter.createProgram("./tsconfig.json");
  gulp.src(['src/**/*.ts'])
    .pipe(gulpTslint({
      configuration: 'tslint.json',
      program: program,
      formatter: 'stylish'
    }))
    .pipe(gulpTslint.report({
      summarizeFailureOutput: true
    }));
  cb();
});

const serverCompiler = typescript.createProject('./tsconfig.json');
gulp.task('compile', (cb) => {
  gulp.src(['src/**/*.ts'])
    .pipe(sourcemaps.init())
    .pipe(serverCompiler())
    .pipe(sourcemaps.write('.', {
      sourceRoot: (file) => {
        return file.cwd + '/src'
      }
    }))
    .pipe(gulp.dest('build/'));
  cb();
});

gulp.task('watch', (cb) => {
  gulp.watch(['src/**/*.ts'], gulp.series('compile'));
});


gulp.task('default', gulp.series('watch'));
